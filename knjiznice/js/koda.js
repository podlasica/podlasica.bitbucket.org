
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

/*global $*/
/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  var ehrId = "";
  var sessionId=getSessionId();
  $.ajaxSetup({
      headers: {"Ehr-Session": sessionId}
  });
  $.ajax({
      url: baseUrl+"/ehr",
      type: 'POST',
      success: function(data){
          var ehrId = data.ehrId;
          console.log("EhrId: " + ehrId);
          if(stPacienta==1){
              var partyData = {
                firstNames: "Sessel",
                lastNames: "Hesselhoff",
                dateOfBirth: "1998-05-25",
                partyAdditionalInfo: [
                    {
                        key: "ehrId",
                        value: ehrId
                    }
                ]
              };
              var podatki={
                "ctx/language": "en",
		        "ctx/territory": "SI",
		        "vital_signs/height_length/any_event/body_height_length": 182,
		        "vital_signs/body_weight/any_event/body_weight": 80,
		        "vital_signs/blood_pressure/any_event/systolic": 109,
		        "vital_signs/blood_pressure/any_event/diastolic": 76,
                "vital_signs/indirect_oximetry:0/spo2|numerator": 0.93
              };
          }
          if(stPacienta==2){
              var partyData = {
                firstNames: "Minister",
                lastNames: "Westminister",
                dateOfBirth: "1962-10-08",
                partyAdditionalInfo: [
                    {
                        key: "ehrId",
                        value: ehrId
                    }
                ]
              };
              var podatki={
                "ctx/language": "en",
		        "ctx/territory": "SI",
		        "vital_signs/height_length/any_event/body_height_length": 172,
		        "vital_signs/body_weight/any_event/body_weight": 73,
		        "vital_signs/blood_pressure/any_event/systolic": 125,
		        "vital_signs/blood_pressure/any_event/diastolic": 82,
                "vital_signs/indirect_oximetry:0/spo2|numerator": 0.9
              };
          }
          if(stPacienta==3){
              var partyData = {
                firstNames: "Hassan",
                lastNames: "ibn Saba",
                dateOfBirth: "1050-06-12",
                partyAdditionalInfo: [
                    {
                        key: "ehrId",
                        value: ehrId
                    }
                ]
              };
              var podatki={
                "ctx/language": "en",
		        "ctx/territory": "SI",
		        "vital_signs/height_length/any_event/body_height_length": 156,
		        "vital_signs/body_weight/any_event/body_weight": 56,
		        "vital_signs/blood_pressure/any_event/systolic": 140,
		        "vital_signs/blood_pressure/any_event/diastolic": 89,
                "vital_signs/indirect_oximetry:0/spo2|numerator": 0.73
              };
          }
          var parametri = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: 'Zgodovina Kranjska'
          };
          var idji = ["#armadillo","#beargrill","#carmelita"];
          $.ajax({
              url: baseUrl + "/demographics/party",
              type: 'POST',
              contentType: 'application/json',
              data: JSON.stringify(partyData),
              success: function (party){
                  if(party.action == 'CREATE'){
                      console.log("Success");
                  }
              },
              error: function(err){
                  console.log("Error: "+err);
              }
          });
          $.ajax({
		    url: baseUrl + "/composition?" + $.param(parametri),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $(idji[stPacienta-1]).html("<strong>"+partyData.firstNames+" "+partyData.lastNames+"</strong> : "+
		        ehrId);
		    },
		    error: function(err) {
		    	console.log("Error: "+err);
            }
          });
      }
  });
  return ehrId;
}

function prikazPodatkov() {
  var sessionId=getSessionId();
  var ehrId=$("#meritveVitalnihZnakovEHRid").val();
  $.ajax({
    url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	  type: 'GET',
	  headers: {"Ehr-Session": sessionId},
    success: function (data) {
      var party=data.party;
      $("#a1").html("<table class='table table-striped " +
        "table-hover'><tr>" +
        "<td>"+ehrId+"</td></tr><table>");
      $("#b1").html("<table class='table table-striped " +
        "table-hover'><tr><td>" +
        party.firstNames+"</td></tr></table>");
      $("#c1").html("<table class='table table-striped " +
        "table-hover'><tr><td>"+party.lastNames+"</td></tr></table>");
    }
  });
  $.ajax({
    url: baseUrl + "/view/" + ehrId + "/" + "height_length",
    type: 'GET',
    headers: {"Ehr-Session": sessionId},
    success: function (res) {
      if (res.length > 0) {
      var results = "<table class='table table-striped " +
        "table-hover'>";
      for(var i in res){
        results+="<tr>"+
        "<td class='row'>" + res[i].body_height_length + " " 	+
        res[i].unit + "</td></tr>";
      }
      results+="</table>";
      $("#d1").html(results);
    }
    }
  });
  $.ajax({
    url: baseUrl + "/view/" + ehrId + "/" + "weight",
		type: 'GET',
		headers: {"Ehr-Session": sessionId},
		success: function (res) {
    if (res.length > 0) {
      var results = "<table class='table table-striped " +
        "table-hover'>";
      for(var i in res){
        results+="<tr>"+
        "<td class='row'>" + res[i].weight + " " 	+
        res[i].unit + "</td></tr>";
      }
      results+="</table>";
      $("#e1").html(results);
    }
		}
  });
  $.ajax({
    url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
		type: 'GET',
		headers: {"Ehr-Session": sessionId},
		success: function (res) {
    if (res.length > 0) {
      var results1 = "<table class='table table-striped " +
        "table-hover'>";
        var results2 = "<table class='table table-striped " +
        "table-hover'>";
      for(var i in res){
        results1+="<tr>"+
        "<td class='row'>" + res[i].systolic + " " 	+
        res[i].unit + "</td></tr>";
        results2+="<tr>"+
        "<td class='row'>" + res[i].diastolic + " " 	+
        res[i].unit + "</td></tr>";
      }
      results1+="</table>";
      results2+="</table>";
      $("#f1").html(results1);
      $("#g1").html(results2);
    }
		}
  });
  
  $("#h1").html("<table class='table table-striped " +
    "table-hover'><tr><td>"+"93.5 %"+"</td></tr></table>");
}
//generirajPodatke(1);
//generirajPodatke(2);
//generirajPodatke(3);
// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
